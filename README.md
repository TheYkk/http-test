# To run test

```
docker-compose up -d

hey -z 10s -c 200 http://0.0.0.0:8080/hello

hey -z 10s -c 200 http://0.0.0.0:8081/hello

hey -z 10s -c 200 http://0.0.0.0:8082/hello

```


# Deno + dinatra http (PORT = 8080)
https://github.com/syumai/dinatra

> Requests/sec:	9211.2002

![deno](./img/3.png "Deno")

# JavaScript Fastify (PORT = 8081)
https://www.fastify.io/

> Requests/sec:	13680.8725

![fastify](./img/1.png "Fastify")

# Golang fasthttp (PORT = 8082)
https://github.com/valyala/fasthttp/

>  Requests/sec:	48866.3255

![fasthttp](./img/3.png "Fasthttp")