// ? Get Http server
const fastify = require('fastify')({logger : false});

// ? Healt router
fastify.get('/hello', (_req, reply) => {
  reply.send('hello');
});

// ? Run the server!
fastify.listen(8081, '0.0.0.0', err => {
  if (err) throw err;
});
